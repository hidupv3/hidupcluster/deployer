function banner() {
    echo -e "\e[94m**************************************************************************\e[0m"
    echo -e "\e[33m${1}"
    echo -e "\e[94m**************************************************************************\e[0m"
}
function error() {
    echo
    echo -e "\e[31mERROR!!! $@\e[0m"
    echo
    exit 1
}

banner "Running Hidup Deployer Tools. Version $(cat version.txt)"