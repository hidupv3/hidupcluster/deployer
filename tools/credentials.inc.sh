function generate_credentials() {    
    name=$1

    banner "Generating credentials for $name."    

    CREDENTIALS_DIR="../private/credentials"
    [[ ! -d "${CREDENTIALS_DIR}" ]] && mkdir -p "${CREDENTIALS_DIR}"

    SERVICE_ACCOUNT_NAME=${name}-service-account
    SYS_CONTEXT=$(kubectl config current-context)
    SYS_NAMESPACE=${client_hidup}-system

    CONTEXT=${name}
    NAMESPACE=${name}

    KUBECONFIG_FILE="${CREDENTIALS_DIR}/${name}-$(kubectl config current-context | sed -e 's|.*@||' -e 's|.*/||').yaml"

    SECRET_NAME=$(kubectl get serviceaccount ${SERVICE_ACCOUNT_NAME} \
                    --context ${SYS_CONTEXT} \
                    --namespace ${SYS_NAMESPACE} \
                    -o jsonpath='{.secrets[0].name}' 2> /dev/null)

    [[ -z "$SECRET_NAME" ]] && error "Service account ${SERVICE_ACCOUNT_NAME} not found."

    TOKEN_DATA=$(kubectl get secret ${SECRET_NAME} \
                    --context ${SYS_CONTEXT} \
                    --namespace ${SYS_NAMESPACE} \
                    -o jsonpath='{.data.token}')

    TOKEN=$(echo ${TOKEN_DATA} | base64 -d)

    # Create dedicated kubeconfig
    # Create a full copy
    kubectl config view --raw > "${KUBECONFIG_FILE}".full.tmp
    # # Switch working context to correct context
    # kubectl --kubeconfig "${KUBECONFIG_FILE}".full.tmp config use-context ${CONTEXT}
    # Minify
    kubectl --kubeconfig "${KUBECONFIG_FILE}".full.tmp \
        config view --flatten --minify > "${KUBECONFIG_FILE}".tmp
    # Rename context
    kubectl config --kubeconfig "${KUBECONFIG_FILE}".tmp \
        rename-context ${SYS_CONTEXT} ${CONTEXT}
    # Create token user
    kubectl config --kubeconfig "${KUBECONFIG_FILE}".tmp \
        set-credentials ${CONTEXT}-${NAMESPACE}-token-user \
        --token ${TOKEN}
    # Set context to use token user
    kubectl config --kubeconfig "${KUBECONFIG_FILE}".tmp \
        set-context ${CONTEXT} --user ${CONTEXT}-${NAMESPACE}-token-user
    # Set context to correct namespace
    kubectl config --kubeconfig "${KUBECONFIG_FILE}".tmp \
        set-context ${CONTEXT} --namespace ${NAMESPACE}
    # Flatten/minify kubeconfig
    kubectl config --kubeconfig "${KUBECONFIG_FILE}".tmp \
        view --flatten --minify > "${KUBECONFIG_FILE}"
    
    chmod 600 "${KUBECONFIG_FILE}"
    # Remove tmp
    rm "${KUBECONFIG_FILE}".full.tmp
    rm "${KUBECONFIG_FILE}".tmp

    echo "Generate "${KUBECONFIG_FILE}""
    echo $PWD
}
