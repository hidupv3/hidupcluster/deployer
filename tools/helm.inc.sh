### Download helm charts
function download_charts() {
    banner "Downloading helm charts"
    if [[ -z "${helm_repo}" ]]
    then
        git clone git@gitlab.com:hidupv3/hidupcluster/helm.git /tmp/helm
    else   
        git clone ${helm_repo} /tmp/helm
    fi
    [[ "$?" != 0 ]] && error
}