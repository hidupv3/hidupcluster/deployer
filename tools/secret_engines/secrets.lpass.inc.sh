### Save secrets
function save_secrets_lpass() {
    source deploy.cfg
    lpass_folder="${lpass_base}/${name}"
    banner "Saving secrets to ${lpass_folder}"
    lpass ls "${lpass_folder}" > /dev/null 2> /dev/null
    if [[ "$?" != 0 ]]
    then
        lpass login ${lpass_login}
        [[ "$?" != 0 ]] && error
    fi
    for f in $(configure " " private/secrets private/credentials)
    do
        secret="${lpass_folder}/$(echo ${f} | sed -e 's|/|_|g' -e 's|\.\.||')" 
        [[ $(lpass ls "${secret}" | wc -l) == "0" ]] && action="add" || action="edit"
        echo "[${action}-$(lpass ls "${secret}" | wc -l)] Uploading ${secret}"
        lpass ${action} --non-interactive --notes "${secret}" < ${f}
        [[ "$?" != 0 ]] && error
    done
}