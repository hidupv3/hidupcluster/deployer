source tools/system.inc.sh

### Deploy runner
function deploy_runner() {
    banner "Deploying runner"
    echo "--> with $(configure -f values private/secrets)"
    source deploy.cfg
    helm upgrade ${name} -n ${client_hidup}-runners /tmp/helm/hidup-runner $(configure -f values private/secrets) "$@"
    [[ "$?" != 0 ]] && error
}

### Deploy chart
function deploy_chart() {
    banner "Deploying chart"
    echo "--> with $(configure -f values private/secrets)"
    source deploy.cfg
    helm upgrade ${name} -n ${client_hidup}-system /tmp/helm/${helm_chart} --set=client=${client_hidup} $(configure -f values private/secrets) "$@"
    [[ "$?" != 0 ]] && error
}
