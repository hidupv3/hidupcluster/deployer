### System tools
function clean_all() {
    rm -rf /tmp/helm
}

function generate_key() {
    echo $(cat /dev/urandom | tr -dc '[:alpha:]' | fold -w ${1:-1024} | head -n 1)
}

### Configure
function configure() {
    limit=${1} 
    shift
    for d in $@
    do
        if [[ -d ../${d} ]]
        then
            for f in $(find ../$d -type f -name "*.y*ml" -exec echo ${limit} {} \;)
            do
                OPTS="${OPTS} ${f}"
            done
        fi 
    done
    
    echo "$OPTS"
}