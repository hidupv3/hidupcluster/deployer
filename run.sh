#!/bin/bash

#git clone git@gitlab.com:hidupv3/hidupcluster/deployer.git
git clone https://gitlab.com/hidupv3/hidupcluster/deployer.git
cp deploy.cfg deployer
cd deployer

./deployer.sh $@

cd .. && rm -rf deployer
