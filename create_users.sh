#!/bin/bash

source tools/banner.inc.sh
source tools/credentials.inc.sh

[[ -z "$1" ]] && error "User not found" || export user=$1
shift
[[ -z "$1" ]] && error "Namespace not found" || export namespace_root="$1"

banner "Creating service account"
envsubst < yaml/serviceaccount.yaml | kubectl apply -f -
banner "Creating RBAC"
export namespace=${namespace_root}-develop
envsubst < yaml/rbac.yaml | kubectl apply -f -
for i in "staging" "production"
do
    export namespace=${namespace_root}-${i}
    envsubst < yaml/rbac-ro.yaml | kubectl apply -f -
done

generate_credentials $user

