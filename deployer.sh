#!/bin/bash

source tools/system.inc.sh
source tools/banner.inc.sh
source tools/credentials.inc.sh
source tools/deployers.inc.sh
source tools/helm.inc.sh
source tools/secrets.inc.sh

if [[ ! -f "deploy.cfg" ]] 
then
    error "deploy.cfg not found"
    exit 1
else
    source deploy.cfg

    current_cluster=$(kubectl config view -o jsonpath='{.clusters[].name}' | sed 's|.*/||')

    [[ "${current_cluster}" != "${cluster}" ]] && error "Expected cluster ${cluster}, current cluster ${current_cluster}."

    banner "Using this configuration"
    cat deploy.cfg
    echo
    echo
    echo "Applyng in 15 seconds, control+c to stop..."

    [[ -z ${NOSLEEP} ]] && sleep 15

fi

### Main
clean_all
download_charts
[[ -z "${disable_runners}" ]] && deploy_runner "$@"
deploy_chart "$@"
generate_credentials ${name}
save_secrets 
clean_all